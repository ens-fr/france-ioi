# Feu de forêt, version A

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=0)


On indique en commentaire à partir d'où on reprend le code.

```python
from lib import *

ecrireDate(1, 2, 0)

ecrireDate(1, 1, 1)
ecrireDate(0, 2, 1)

ecrireDate(1, 0, 2)
ecrireDate(2, 1, 2)
ecrireDate(0, 3, 2)

# on complète ici
ecrireDate(0, 0, 3)
ecrireDate(2, 0, 3)
ecrireDate(3, 1, 3)
ecrireDate(0, 4, 3)
```

