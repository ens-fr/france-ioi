# Billes, version D

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=8)

Dans cette version, on lance plusieurs fois une bille à des positions données différentes, en la retirant du plateau à la fin de son parcours.

## Version lente

On reprend le code de la version B, et on l'adapte un peu.

```python
from lib import *

for iLancer in range(nbLancers()):
    x = marcheLancer(iLancer)
    while(hauteur(x) >= hauteur(x + 1)):
        x += 1

    positionFinale(x)
```

## Version efficace

!!! tip "Objectif"
    Ne pas faire de double boucle et répondre instantanément pour chaque requête.

On construit au début la liste des réponses en fonction de la position de départ.

* Pour $x$ le dernier indice de marche, on n'a pas de réponse, mais on nous garantit que la dernière marche suffit à arrêter la bille dans le pire des cas.
* La réponse `x_fin` s'initialise donc dès le premier tour de boucle à l'avant-dernière marche, et on progresse vers la gauche pour mettre à jour au cas où.

```python
from lib import *

# initialisation de toutes les réponses
nb_marches = nbMarches()
réponses = [nb_marches - 2 for x in range(nb_marches)]
réponses[nb_marches - 1] = None  # inutilisé

x_fin = nb_marches - 2
for x in range(nb_marches - 3, -1, -1):
    if hauteur(x + 1) > hauteur(x):
        x_fin = x
    réponses[x] = x_fin

# Réponses données rapidement
for iLancer in range(nbLancers()):
    x_0 = marcheLancer(iLancer)
    x_fin = réponses[x_0]
    positionFinale(x_fin)
```

Pourquoi `-3` dans `#!py3 for x in range(nb_marches - 3, -1, -1):` ?

- `réponses[nb_marches - 1]` a déjà été initialisé à `None`.
- `réponses[nb_marches - 2]` a déjà été initialisé à `nb_marches - 2`.
- On construit les autres réponses...
