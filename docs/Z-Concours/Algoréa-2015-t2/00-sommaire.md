# Concours Algoréa 2015 : Tour 2

[Liste des questions](http://www.france-ioi.org/algo/chapter.php?idChapter=925)

Cette épreuve dure 2 h 30 consécutives et est composée de deux problèmes, chacun découpé en 5 versions.

**Difficulté** : la version A de chaque problème est facile, la version B un peu moins, etc. et la version E est relativement difficile. N'oubliez donc pas de passer du temps sur les versions faciles du deuxième problème pour assurer des points, avant d'attaquer les versions difficiles du premier problème.

**Score** : chaque version est notée sur 100 points, donc l'ensemble de l'épreuve est sur 1000 points. Le nombre de points que vous obtiendrez sur une version sera basé sur le nombre de tests que votre programme réussit. Vous pouvez faire autant d'essais que vous le souhaitez sur chaque sujet.
