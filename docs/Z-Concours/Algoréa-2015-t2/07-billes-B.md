# Billes, version B

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=6)

Dans cette version, on lance **la** bille à l'extrémité gauche du plateau, c'est-à-dire la marche 0. Vous devez écrire un programme qui, étant donné les hauteurs des marches, détermine jusqu'où la bille va aller. 

## Une solution

On part de la gauche $x=0$, et tant que la fonction est décroissante, on va vers la droite.

```python
from lib import *

x = 0
while hauteur(x) >= hauteur(x + 1):
    x += 1

positionFinale(x)
```
