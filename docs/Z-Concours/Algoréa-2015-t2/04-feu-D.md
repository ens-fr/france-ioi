# Feu de forêt, version D

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=3)

## Jour après jour

Une solution où on marque jour après jour, avec une lecture de la grille entière par jour.

```python
from lib import *

taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()
ecrireDate(i_0, j_0, 0)  # jour 0

for date in range(2 * taille - 1):
    for i in range(taille):
        for j in range(taille):
            if lireDate(i, j) == date:
                # ça pourrait bruler demain en bas
                if not estObstacle(i+1, j):
                    ecrireDate(i+1, j, date + 1)
                # ça pourrait bruler demain à droite
                if not estObstacle(i, j+1):
                    ecrireDate(i, j+1, date + 1)
```

## Récursif

Une solution récursive efficace.

!!! warning "Attention"
    Il faut vérifier qu'on n'a pas déjà écrit la date, sinon le programme est trop lent. Il refait plusieurs fois le même travail...

```python
from lib import *

def marque(i, j, date):
    ecrireDate(i, j, date)
    # ça pourrait bruler demain en bas
    if not estObstacle(i+1, j):
        if lireDate(i+1, j) == -1:
            marque(i+1, j, date + 1)
    # ça pourrait bruler demain à droite
    if not estObstacle(i, j+1):
        if lireDate(i, j+1) == -1:
            marque(i, j+1, date + 1)

taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()
marque(i_0, j_0, 0)  # jour 0
```
