# Billes, version C

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=7)

Dans cette version, on lance plusieurs billes successivement, à l'extrémité gauche du plateau (marche 0). À la fin de leur parcours, on laisse les billes là où elles s'arrêtent : elles ajoutent 1 à la hauteur de la marche sur laquelle elles se sont arrêtées.

## Une solution

On a besoin de stocker les nouvelles hauteurs pour les mettre à jour quand de nouvelles billes s'arrêtent.

En reprenant le code précédent, pour chaque bille, on utilise le tableau des hauteurs au lieu de la fonction, et on pense à la mettre à jour.

```python
from lib import *

# initialisation de toutes les hauteurs
hauteurs = [hauteur(x) for x in range(nbMarches())]

for _ in range(nbLancers()):
    x = 0
    while hauteurs[x] >= hauteurs[x + 1]:
        x += 1

    hauteurs[x] += 1
    positionFinale(x)
```
