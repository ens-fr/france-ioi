# Billes, version E

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=9)

Dans cette version, on lance plusieurs billes successivement, à l'extrémité gauche du plateau (marche 0). À la fin de leur parcours, on laisse les billes là où elles s'arrêtent : elles ajoutent 1 à la hauteur de la marche sur laquelle elles se sont arrêtées.

---

On reprend le code de la version C. À chaque nouveau lancer, on sait qu'on pourra aller au moins jusqu'à la dernière position, **moins un**.

!!! warning "Attention"
    Cette position minimale atteinte ne peut pas être négative, ce sera au moins zéro ! D'où le dernier test.


```python
from lib import *

# initialisation de toutes les hauteurs
hauteurs = [hauteur(x) for x in range(nbMarches())]
x = 0  # lancer initial

for _ in range(nbLancers()):
    while hauteurs[x] >= hauteurs[x + 1]:
        x += 1

    hauteurs[x] += 1
    positionFinale(x)
    if x > 0:
        x -= 1
    # la bille suivante ira au moins jusqu'à x
```
