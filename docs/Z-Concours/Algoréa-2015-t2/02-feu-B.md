# Feu de forêt, version B

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=1)

* On a simplifié le code proposé.
* `i` et `j` sont de bons noms de variable pour les coordonnées dans une grille, une image ou une matrice.

```python
from lib import *

i = ligDepart()
j = colDepart()
date = 0

while not estObstacle(i, j):
    ecrireDate(i, j, date)
    j -= 1  # on va à gauche
    date += 1
```

