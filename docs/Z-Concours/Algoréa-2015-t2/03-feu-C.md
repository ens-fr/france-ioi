# Feu de forêt, version C

[sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=2)

## D'après le code proposé

Une solution dans l'esprit du code proposé en exemple.

```python
from lib import *
 
taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()

j = j_0
date = 0
while not estObstacle(i_0, j):
    ecrireDate(i_0, j, date)
    j += 1
    date += 1

for j in range(taille):
    date = lireDate(i_0, j)
    if date != -1:
        i = i_0
        while not estObstacle(i, j):
            ecrireDate(i, j, date)
            i += 1
            date += 1
```

## Calcul de la date

Une solution avec un peu de maths ; on calcule la date en fonction des coordonnées. Cette solution n'est pas pérenne en cas d'obstacle...

```python
from lib import *

taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()

for i in range(i_0, taille):
    for j in range(j_0, taille):
        date = i + j - i_0 - j_0
        ecrireDate(i, j, date)
```
