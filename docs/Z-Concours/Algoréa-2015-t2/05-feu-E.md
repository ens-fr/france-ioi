# Feu de forêt, version E

[Sujet](http://www.france-ioi.org/algo/task.php?idChapter=925&iOrder=4)

## Récursif

Reprise de la version récursive du D.

!!! warning "Attention"
    Il faut mettre à jour aussi si ça brule avant une première fois marqué.

```python
from lib import *

def marque(i, j, date):
    ecrireDate(i, j, date)
    # ça pourrait bruler demain en bas
    if not estObstacle(i+1, j):
        if lireDate(i+1, j) == -1 or lireDate(i+1, j) > date + 1:
            marque(i+1, j, date + 1)
    # ça pourrait bruler demain à droite
    if not estObstacle(i, j+1):
        if lireDate(i, j+1) == -1 or lireDate(i, j+1) > date + 1:
            marque(i, j+1, date + 1)
    # ça pourrait bruler demain en haut
    if not estObstacle(i-1, j):
        if lireDate(i-1, j) == -1 or lireDate(i-1, j) > date + 1:
            marque(i-1, j, date + 1)
    # ça pourrait bruler demain à gauche
    if not estObstacle(i, j-1):
        if lireDate(i, j-1) == -1 or lireDate(i, j-1) > date + 1:
            marque(i, j-1, date + 1)

taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()
marque(i_0, j_0, 0)  # jour 0
```
## Récursif, factorisé

```python
from lib import *

def marque(i, j, date):
    ecrireDate(i, j, date)
    for di, dj in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
        idi, jdj = i + di, j + dj
        if not estObstacle(idi, jdj):
            if lireDate(idi, jdj) == -1 or lireDate(idi, jdj) > date + 1:
                marque(idi, jdj, date + 1)

taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()
marque(i_0, j_0, 0)  # jour 0
```

## Jour après jour

!!! warning "Plus lent"
    Il peut y avoir des trajets qui serpentent, donc la durée du feu peut être d'environ `taille` au carré. La boucle est bien plus lente.

```python
from lib import *

def brule_aujourdhui(i, j, date):
    if estObstacle(i, j):
        return False
    for di, dj in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
        idi, jdj = i + di, j + dj
        if lireDate(idi, jdj) == date - 1:
            # à côté, ça a brulé hier !
            return lireDate(i, j) == -1  # ça n'a pas encore brulé
    return False

taille = tailleCote()
i_0 = ligDepart()
j_0 = colDepart()
ecrireDate(i_0, j_0, 0)  # jour 0

for date in range(1, taille*(taille+1)//2):
    for i in range(taille):
        for j in range(taille):
            if brule_aujourdhui(i, j, date):
                ecrireDate(i, j, date)
```
