# FranceIOI

On propose ici des solutions pour France-IOI en Python pour les élèves de NSI (surtout en terminale).

Des variantes de code présentent :

- souvent un style fonctionnel,
- parfois avec de la récursivité,
- ou bien des méthodes propres à *Python*,
- ou encore des méthodes générales.

!!! tip "Pour progresser"
    Ces solutions alternatives du professeur, avec commentaires, sont à étudier après avoir déjà résolu le problème.

!!! danger "Quelques difficultés"

    Dans le niveau 3, Efficacité temporelle

    - Les problèmes 2 et 3 sont accessibles.

    - ⚠️ **Ne pas faire encore le 1 et 4 ; difficiles**

    ??? tip "Lecture rapide"
        Il faut apprendre à savoir lire l'entrée de manière très rapide. Voici une méthode rapide, mais qui lit les lignes en lui ajoutant sa fin de ligne, souvent un saut de ligne. Ce n'est pas gênant pour ensuite transtyper en entier.

        ```python
        import sys
        def main(input=sys.stdin.readline)
            # votre code ici
        main()
        ```
