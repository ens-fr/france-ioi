# 93%

import sys
def main():
    n = int(input())
    nombres = [int(input()) for _ in range(n)]
    cumul_1 = [0] * n
    cumul_1[1] = 1
    q = 1
    for i in range(2, n):
        if nombres[i-2] < nombres[i-1]:
            q += 1
        else:
            q = 1
        cumul_1[i] = q

    cumul_2 = [0] * n
    cumul_2[n-2] = 1
    q = 1
    for i in range(n-3, -1, -1):
        if nombres[i+1] < nombres[i+2]:
            q += 1
        else:
            q = 1
        cumul_2[i] = q
    best = max(cumul_1[n-1], cumul_2[0])
    for i in range(1, n-1):
        if nombres[i-1] < nombres[i+1]:
            best = max(best, cumul_1[i] + cumul_2[i])
    print(best)
main()
