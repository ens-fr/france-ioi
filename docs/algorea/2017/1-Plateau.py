# 100%
n = int(input())
y = -1
cumul = 0
best = 0
champ = 0
for _ in range(n):
    x = int(input())
    if x == y:
        cumul += 1
        if cumul > best:
            best = cumul
            champ = x
    else:
        y = x
        cumul = 1
print(champ)
