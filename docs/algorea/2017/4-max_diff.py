n = int(input())
l = int(input())
presences = [0] * (n+1)
nb_presents = 0

file = [0] * l

for i in range(l):
    x = int(input())
    file[i] = x
    if presences[x] == 0:
        nb_presents += 1
    presences[x] += 1
i+=1

best = nb_presents

for j in range(n - l):
    #print(nb_presents, file, presences[:6])
    if i == l:
        i = 0
    x = int(input())
    y = file[i]
    #print(x, y)
    if x != y:
        presences[y] -= 1
        if presences[y] == 0:
            nb_presents -= 1
        if presences[x] == 0:
            nb_presents += 1
        presences[x] += 1
    file[i] = x
    i += 1
    if best < nb_presents:
        best = nb_presents
print(best)
