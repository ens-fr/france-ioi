# 100%
n = int(input())
best = 41000
derniers = [-1] * 41000
for i in range(n):
    x = int(input())
    j = derniers[x]
    if j != -1:
        z = i - j
        if z < best:
            best = z
    derniers[x] = i
print(best)
