# 88%

n = int(input())
nombres = [int(input()) for _ in range(n)]
ajout = [0] * (n+1)

presents = [0] * (n+1)
for i in range(n):
    presents[nombres[i]] = 1

x = nombres[0]
i = 1
while (i < n) and nombres[i] == x:
    i+=1
if i < n:
    y = nombres[i]
    i += 1
    while (i < n) and nombres[i] == y:
        i+=1
    if i < n:
        z = nombres[i]
        if x == z:
            ajout[y] += 1
        while i < n:
            while (i < n) and nombres[i] == z:
                i += 1
            if i < n:
                x, y, z = y, z, nombres[i]
                if x == z:
                    ajout[y] += 1

#repet = [0] * (n+1)
total = 0
for i in range(n-1):
    if nombres[i] == nombres[i+1]:
        ajout[nombres[i]] -= 1
        total += 1

print(total + min(ajout[i] for i in range(1, n+1) if presents[i]))
